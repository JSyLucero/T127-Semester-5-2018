package com.example.jsylucero.lab5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MyListActivity extends AppCompatActivity {
    private ListView listView;

    ArrayList<String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list);

        data = new ArrayList<>();
        data.add("test1");
        data.add("test2");
        data.add("another test");

        listView = findViewById(R.id.myList);
        ArrayAdapter<String> myAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_2, data);
        listView.setAdapter(myAdapter);
    }
}
