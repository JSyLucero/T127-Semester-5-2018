package jsylucero.a1.lab2.com.lab21;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = (Button) findViewById(R.id.button);
        b.setOnClickListener(this);

        b.setText("Submit");
        Button b2 = new Button(this);
        b2.setText("B2");
        b2.setId(12345);
        ((LinearLayout)b.getParent()).addView(b2);
    }

    @Override
    public void onClick(View v) {
        EditText et = findViewById(R.id.editText);
        TextView tv = findViewById(R.id.textView);

        int meters = Integer.parseInt(et.getText().toString());
        float inches = meters * 100 / 2.5f;

        tv.setText(""+inches);
    }
}
