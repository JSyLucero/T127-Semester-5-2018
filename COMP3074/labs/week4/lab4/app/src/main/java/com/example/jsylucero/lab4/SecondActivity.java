package com.example.jsylucero.lab4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Log.d("TEST","OnCreate-Second");
        findViewById(R.id.button2).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //Intent i = new Intent(this, MainActivity.class);
        //startActivity(i); DO NOT DO THIS ONLY USE finish()
        Intent res = getIntent();
        res.putExtra("TAG","This is my message");
        this.setResult(RESULT_OK, res);

        finish();
    }
}
