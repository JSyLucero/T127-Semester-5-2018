package com.example.jsylucero.lab4;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    protected int count;
    static final int PICK_CONTACT_REQUEST = 0;
    static final int GET_MSG_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        count++;
        Log.d("TEST","onCreate("+count+")");
        Button b = findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), SecondActivity.class);
                v.getContext().startActivity(i);
                Log.d("TEST","onClick()");
        }
        });

        Button b3 = findViewById(R.id.button3);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
                i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(i, PICK_CONTACT_REQUEST);
            }
        });

        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("geo:0,0?q=160+kendal+ave+toronto"));
                startActivity(i);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri contactUri = data.getData();
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                Cursor c = getContentResolver().query(contactUri, projection, null, null, null);
                c.moveToFirst();
                int col = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = c.getString(col);
                Log.d("TEST","Phone Number: " + number);
            }
        } else if (requestCode == GET_MSG_REQUEST) {
            String s = data.getStringExtra("TAG");
            Log.d("TEST",s);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("TEST","onStart()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TEST","onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TEST","onResume()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("TEST","onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("TEST","onDestroy()");
    }
}
