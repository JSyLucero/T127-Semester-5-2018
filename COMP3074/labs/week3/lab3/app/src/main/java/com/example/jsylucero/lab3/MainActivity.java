package com.example.jsylucero.lab3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView textView;
    Button btn_0, btn_1, btn_2,
            btn_3, btn_4, btn_5,
            btn_6, btn_7, btn_8,
            btn_9, btn_Clear, btn_Add,
            btn_Subtract, btn_Multiply, btn_Divide,
            btn_Decimal, btn_Equal;

    float value1, value2;
    enum Operation {
        none,
        add,
        subtract,
        multiply,
        divide
    }
    Operation op = Operation.none;

    private View.OnClickListener number_onclick = new View.OnClickListener() {
      @Override
      public void onClick(View v) {

      }
    };

    private float calculate() {
        switch (op) {
            case add: return value1 + value2;
            case subtract: return value1 - value2;
            case multiply: return value1 * value2;
            case divide:
                if (value2 > 0)
                    return value1 / value2;
                else
                    return Float.NaN;
            case none: return value1;
            default: return Float.NaN;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);

        btn_0 = findViewById(R.id.btn_0);
        btn_1 = findViewById(R.id.btn_1);
        btn_2 = findViewById(R.id.btn_2);
        btn_3 = findViewById(R.id.btn_3);
        btn_4 = findViewById(R.id.btn_4);
        btn_5 = findViewById(R.id.btn_5);
        btn_6 = findViewById(R.id.btn_6);
        btn_7 = findViewById(R.id.btn_7);
        btn_8 = findViewById(R.id.btn_8);
        btn_9 = findViewById(R.id.btn_9);

        btn_Clear = findViewById(R.id.btn_Clear);
        btn_Add = findViewById(R.id.btn_Add);
        btn_Subtract = findViewById(R.id.btn_Subtract);
        btn_Multiply = findViewById(R.id.btn_Multiply);
        btn_Decimal = findViewById(R.id.btn_Decimal);
        btn_Divide = findViewById(R.id.btn_Divide);
        btn_Equal = findViewById(R.id.btn_Equal);

        btn_0.setOnClickListener(this);
        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
        btn_4.setOnClickListener(this);
        btn_5.setOnClickListener(this);
        btn_6.setOnClickListener(this);
        btn_7.setOnClickListener(this);
        btn_8.setOnClickListener(this);
        btn_9.setOnClickListener(this);

        btn_Decimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button)v;
                if (!textView.getText().toString().contains("."))
                    textView.setText(textView.getText() + b.getText().toString());
            }
        });

        btn_Clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value1 = 0;
                value2 = 0;
                op = Operation.none;
                textView.setText("0");
            }
        });

        btn_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (op == Operation.none) {
                    op = Operation.add;
                    value1 = Float.parseFloat(textView.getText().toString());
                    textView.setText("0");
                } else {
                    value2 = Float.parseFloat(textView.getText().toString());
                    value1 = calculate();
                }
            }
        });

        btn_Subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (op == Operation.none) {
                    op = Operation.subtract;
                    value1 = Float.parseFloat(textView.getText().toString());
                    textView.setText("0");
                } else {
                    value2 = Float.parseFloat(textView.getText().toString());
                    value1 = calculate();
                    op = Operation.add;
                    textView.setText("0");
                }
            }
        });

        btn_Subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (op == Operation.none) {
                    op = Operation.subtract;
                    value1 = Float.parseFloat(textView.getText().toString());
                    textView.setText("0");
                } else {
                    value2 = Float.parseFloat(textView.getText().toString());
                    value1 = calculate();
                    op = Operation.subtract;
                    textView.setText("0");
                }
            }
        });

        btn_Multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (op == Operation.none) {
                    op = Operation.multiply;
                    value1 = Float.parseFloat(textView.getText().toString());
                    textView.setText("0");
                } else {
                    value2 = Float.parseFloat(textView.getText().toString());
                    value1 = calculate();
                    op = Operation.multiply;
                    textView.setText("0");
                }
            }
        });

        btn_Divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (op == Operation.none) {
                    op = Operation.divide;
                    value1 = Float.parseFloat(textView.getText().toString());
                    textView.setText("0");
                } else {
                    value2 = Float.parseFloat(textView.getText().toString());
                    value1 = calculate();
                    op = Operation.divide;
                    textView.setText("0");
                }
            }
        });

        btn_Equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (op == Operation.none)
                    return;

                value2 = Float.parseFloat(textView.getText().toString());
                value1 = calculate();
                textView.setText(value1+"");
                op = Operation.none;
            }
        });
    }

    public void onClick(View v) {
        Button b = (Button)v;
        if (textView.getText().toString().equals("0"))
            textView.setText("");
        textView.setText(textView.getText() + b.getText().toString());
    }
}
