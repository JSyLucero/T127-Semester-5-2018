package local.jsylucero.project.comp3074_assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class GeneralSchedule extends AppCompatActivity {
    private String[] days_of_week = {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_schedule);

        for (int i = 0; i < days_of_week.length; i++) {
            int id = getResources().getIdentifier("btn"+days_of_week[i], "id", getPackageName());
            findViewById(id).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), MyPlanner.class);
                    startActivity(i);
                }
            });
        }
    }
}
