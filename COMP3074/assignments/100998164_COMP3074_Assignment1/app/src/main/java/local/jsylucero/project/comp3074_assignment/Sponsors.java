package local.jsylucero.project.comp3074_assignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Sponsors extends AppCompatActivity {
    private ArrayList<String> items;
    private ArrayAdapter<String> itemsAdapter;
    private ListView listItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsors);
        listItems = findViewById(R.id.listSponsors);
        items = new ArrayList<>();
        items.add("Starbucks Coffee");
        items.add("Apple");
        items.add("Tesla");

        itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);
        listItems.setAdapter(itemsAdapter);
    }
}
