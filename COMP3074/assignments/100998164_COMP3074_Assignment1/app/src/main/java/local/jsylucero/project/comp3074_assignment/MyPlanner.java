package local.jsylucero.project.comp3074_assignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MyPlanner extends AppCompatActivity {
    private ArrayList<String> items;
    private ArrayAdapter<String> itemsAdapter;
    private ListView listItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_planner);
        listItems = findViewById(R.id.listPlanner);
        items = new ArrayList<>();
        items.add("Standup Meeting @ 11:00AM");
        items.add("Have lunch @ 12:00PM");

        itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);
        listItems.setAdapter(itemsAdapter);

        findViewById(R.id.btnPlan_Add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText schedPlan = findViewById(R.id.txtSchedPlan);
                String text = schedPlan.getText().toString();
                if(!text.isEmpty()){
                    itemsAdapter.add(text);
                    schedPlan.setText("");
                }
            }
        });
    }
}
