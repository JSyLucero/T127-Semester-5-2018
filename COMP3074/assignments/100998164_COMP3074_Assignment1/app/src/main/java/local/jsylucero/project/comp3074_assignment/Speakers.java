package local.jsylucero.project.comp3074_assignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Speakers extends AppCompatActivity {
    private ArrayList<String> items;
    private ArrayAdapter<String> itemsAdapter;
    private ListView listItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speakers);
        listItems = findViewById(R.id.listSpeakers);
        items = new ArrayList<>();
        items.add("Elon Musk");
        items.add("Mark Zuckerberg");

        itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);
        listItems.setAdapter(itemsAdapter);
    }
}
