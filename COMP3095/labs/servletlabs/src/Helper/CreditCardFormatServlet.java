package Helper;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CreditCardFormatServlet
 */
@WebServlet("/CreditCardHelp")
public class CreditCardFormatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreditCardFormatServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter pw = response.getWriter();
        
        pw.println("<!DOCTYPE html>");
        pw.println("<html>");
        pw.println("<head>");
        pw.println("<title>");
        pw.println("Credit Card Help");
        pw.println("</title>");
        pw.println("</head>");
        pw.println("<body>");
        pw.println("<h2>");
        pw.println("Understanding what credit card you have.");
        pw.println("</h2>");
        pw.println("<table>");
        pw.println("<tr>");
        pw.println("<td>Visa</td>");
        pw.println("<td>4111 1111 1111 1111</td>");
        pw.println("</tr>");
        pw.println("<tr>");
        pw.println("<td>MasterCard</td>");
        pw.println("<td>5500 0000 0000 0004</td>");
        pw.println("</tr>");
        pw.println("<tr>");
        pw.println("<td>American Express</td>");
        pw.println("<td>3400 0000 0000 009</td>");
        pw.println("</tr>");
        pw.println("</table>");
        pw.println("</body>");
        pw.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
