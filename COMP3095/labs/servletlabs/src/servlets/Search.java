package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Params
		String name = request.getParameter("searchEngine");
		
		// Redirect paths
		if (name.contains("Google"))
			response.sendRedirect("http://www.google.ca/");
		else if (name.contains("Yahoo"))
			response.sendRedirect("http://www.yahoo.ca/");
		else if (name.contains("Bing"))
			response.sendRedirect("http://www.bing.ca/");
		else
			response.sendError(HttpServletResponse.SC_NOT_FOUND,
					"The search engine is fake and cannot be found.");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
