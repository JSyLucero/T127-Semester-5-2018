package servlets.cookie;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SetCookie
 */
@WebServlet("/SetCookie")
public class SetCookie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SetCookie() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie firstName = new Cookie("first", request.getParameter("firstname"));
		Cookie lastName = new Cookie("last", request.getParameter("lastname"));
		
		// Set expiry date after 24 hours for both the cookies
		firstName.setMaxAge(60*60*24);
		lastName.setMaxAge(60*60*24);
		
		// Add both the cookies in the response header
		response.addCookie(firstName);
		response.addCookie(lastName);
		
		// Set response content type
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		String title = "Setting Cookies Example";
		String docType = "<!DOCTYPE html>\n";
		out.println(docType +
					"<html>\n" +
					"<head><title>" + title + "</title></head>\n" +
					"<body bgcolor=\"#f0f0f0\">\n" +
					"<h1 style='text-align: center;>'" + title + "</h1>\n" +
					"<ul>\n" +
					"\t<li><span style='font-weight: bold;>First Name</span>: '>" +
					request.getParameter("firstname") + "\n" +
					"</li>\n" +
					"\t<li><span style='font-weight: bold;>Last Name</span>: '>" +
					request.getParameter("lastname") + "\n" +
					"</li>\n" +
					"</ul>\n" +
					"</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
