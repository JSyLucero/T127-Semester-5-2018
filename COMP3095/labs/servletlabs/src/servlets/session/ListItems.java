package servlets.session;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ListItems
 */
@WebServlet("/ListItems")
public class ListItems extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListItems() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Create a session object if it is already not created
		HttpSession session = request.getSession();
		
		ArrayList<String> previousItems =
		(ArrayList<String>)session.getAttribute("previousItems");
		if (previousItems == null)
			previousItems = new ArrayList<String>();
		
		String newItem = request.getParameter("newItem");
		if ((newItem != null) && (!newItem.trim().equals("")))
			previousItems.add(newItem);
		
		session.setAttribute("previousItems", previousItems);
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String docType = "<!DOCTYPE html>";
		out.println(docType +
					"<html>\n" +
					"<head><title> List Items </title></head>\n" +
					"<body>\n" +
					"<H1> List of Item </H1>");
		
		if (previousItems.size() == 0)
			out.println("<span style='font-style: italic;'>No items</span>");
		
		else {
			out.println("<ul>");
			for (String item : previousItems)
				out.println("<li>" + item + "</li>");
			out.println("</ul>");
		}
		
		out.println("</body></html>");
	}

}
