package servlets.session;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ShowSession
 */
@WebServlet("/ShowSession")
public class ShowSession extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowSession() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Create a session object if it is already not created
		HttpSession session = request.getSession();
		// Get session creation time
		Date createTime = new Date(session.getCreationTime());
		// Get last access time of this web page
		Date lastAccessTime = new Date(session.getLastAccessedTime());
		
		String heading;
		Integer accessCount = 0;
		String accessCountKey = "AccessCountKey";
		String userID = "COMP3095 - username";
		
		// Check if this is a newcomer to your webpage
		if (session.isNew()) {
			heading = "Welcome, " + userID;
			accessCount = 0;
		}
		
		else {
			heading = "Welcome back";
			accessCount = (Integer)session.getAttribute(accessCountKey);
			accessCount = accessCount + 1;
		}
		session.setAttribute(accessCountKey, accessCount);
		
		// session.invalidate()
		// Set response content type
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String docType = "<!DOCTYPE html>";
		out.println(docType + "<html>\n" +
					"<head><title> Servlet Session tutorial </title></head>\n" +
					"<body>\n" +
					"<h1 style=\"text-align: center;\">" + heading + "</h1>\n" +
					"<table border=\"1\" style=\"text-align: center;\">\n" +
					"<tr>\n" +
					" <th>Session info</th><th>value</th></tr>\n" +
					"<tr>\n" +
					" <td>id</td>\n" +
					" <td>" + session.getId() + "</td></tr>\n" +
					"<tr>\n" +
					" <td>Creation Time</td>\n" +
					" <td>" + createTime +
					" </td></tr>\n" +
					"<tr>\n" +
					" <td>Time of Last Access</td>\n" +
					" <td>" + lastAccessTime +
					" </td></tr>\n" +
					"<tr>\n" +
					"<td>User ID</td>\n" +
					" <td>" + userID +
					" </td></tr>\n" +
					"<tr>\n" +
					" <td>Number of visits</td>\n" +
					" <td>" + accessCount + "</td></tr>\n" +
					"</table>\n" +
					"</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
