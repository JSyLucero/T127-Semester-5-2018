package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Helper.Values;
import Helper.Validator;

/**
 * Servlet implementation class CreditCard
 */
@WebServlet("/CreditCard")
public class CreditCard extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreditCard() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Params
		String submit = request.getParameter("submit");
		if (submit.equals("Help")) {
			response.sendRedirect("creditcardhelp"); return;
		}
		
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String address = request.getParameter("address");
		String creditcard = request.getParameter("creditcard");
		
		if (Values.isMissing(firstname) 
		 || Values.isMissing(lastname)
		 || Values.isMissing(address)
		 || Values.isMissing(creditcard)) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST); return;
		}
		
		String redirect = Validator.GetCreditCardRedirect(creditcard);
		if (!redirect.equals("404_NotFound")) {
			response.sendRedirect(redirect);
		} else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid Credit Card");
		}
	}

}
