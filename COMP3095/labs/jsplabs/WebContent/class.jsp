<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Class JSP</title>
</head>
<body>
	<%@ page import="calculation.basic.*" %>
	<% 
		Calculation c = new Calculation();
		String calcMethod = "square";
		if (request.getParameter("cal") != null)
			calcMethod = request.getParameter("cal");
		
		double num = 2;
		if(request.getParameter("num") != null)
			num = Double.parseDouble(request.getParameter("num"));
		
		double result = num;
		switch (calcMethod) {
			default: case "square":
				result = c.square(num);
				break;
			case "cube":
				result = c.cube(num);
				break;
		}
	%>
	<%= num %> <%= calcMethod + "d" %> is <%= result %>
</body>
</html>