<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Bean JSP</title>
</head>
<body>
	<form>
		<input type="text" placeholder="First Name" name="firstname"/>
		<input type="text" placeholder="Last Name" name="lastname"/>
		<input type="text" placeholder="Age" name="age"/>
		<input type="submit" />
	</form>

	<jsp:useBean id="student" class="person.bean.Student">
		<jsp:setProperty name="student" property="firstName" value='<%= request.getParameter("firstname") == null ? "" : request.getParameter("firstname") %>'/>
		<jsp:setProperty name="student" property="lastName" value='<%= request.getParameter("lastname") == null ? "" : request.getParameter("lastname") %>'/>
		<jsp:setProperty name="student" property="age" value='<%= request.getParameter("age") == null ? 0 : Integer.parseInt(request.getParameter("age")) %>'/>
	</jsp:useBean>
	
	<p>
		Student Name: <jsp:getProperty name="student" property="fullName"/>
	</p>
	
	<p>
		Student Age: <jsp:getProperty name="student" property="age"/>
	</p>
</body>
</html>