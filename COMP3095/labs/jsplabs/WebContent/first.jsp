<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>First JSP Lab</title>
</head>
<body>
	<h1>First JSP</h1>
	<ul>
		<li>Current Time: <%= new java.util.Date() %></li>
		<li>Server:       <%= application.getServerInfo() %></li>
		<li>Session ID:   <%= session.getId() %></li>
		<li>Sesh Created: <%= session.getCreationTime() %>
		<li>App Context:  <%= application.getContextPath() %></li>
		<li>Host OS:      <%= System.getProperty("os.name") %></li>
	</ul>
</body>
</html>