package calculation.basic;

public class Calculation {
	public double square(double x) {
		return Math.pow(x, 2);
	}
	
	public double cube(double x) {
		return Math.pow(x, 3);
	}
}
