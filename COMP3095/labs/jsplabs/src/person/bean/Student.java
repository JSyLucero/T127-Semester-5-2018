package person.bean;

public class Student {
	private String firstName = null;
	private String lastName = null;
	private int age = 0;
	
	public Student() { }
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getFullName() {
		return String.format("%1$s, %2$s", lastName, firstName);
	}
	
	public int getAge() {
		return age;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
}
