<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>MVC Index JSP</title>
</head>
<body>
	<form action="Controller" method="GET">
		Input an integer: <input type="text" name="value">
		<br/><input type="radio" name="method" value="square" checked/> Square
		<br/><input type="radio" name="method" value="factorial"/> Factorial
		<br/><input type="submit" value="Calculate"/>
	</form>
</body>
</html>