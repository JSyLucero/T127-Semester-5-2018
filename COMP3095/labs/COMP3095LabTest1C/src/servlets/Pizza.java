package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Helper.Values;
import Helper.Validator;
import Helper.Confirmation;

/**
 * Servlet implementation class Pizza
 */
@WebServlet("/Pizza")
public class Pizza extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Pizza() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Params
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String postalcode = request.getParameter("postalcode");
		String pizza_size = request.getParameter("pizza_size");
		String pizza_type = request.getParameter("pizza_type");
		String special_instructions = request.getParameter("special_instructions");
		
		if (Values.isMissing(email)
		 || Values.isMissing(address) 
		 || Values.isMissing(postalcode) 
		 || Values.isMissing(pizza_size)
		 || Values.isMissing(pizza_type)) {
			response.sendRedirect("/COMP3095LabTest1C/failed.html");
		} else if (!Validator.ValidateEmail(email) && !Validator.ValidatePostalCode(postalcode)) {
			response.sendRedirect("/COMP3095LabTest1C/failed.html");
		} else {
			int confirmationCode = Confirmation.GenerateCode();
			PrintWriter pw = response.getWriter();
			pw.println("<h1>Order Confirmation</h1>");
			pw.println(String.format("<h2>Your confirmation #: %d</h2>", confirmationCode));
			pw.println("<h2>Your order has been accepted and created</h2>");
		}
	}

}
