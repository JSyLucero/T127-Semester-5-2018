package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Helper.Values;
import Helper.Validator;
import Helper.Credentials;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendError(HttpServletResponse.SC_BAD_REQUEST);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Params
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if (Values.isMissing(username) || Values.isMissing(password))
			response.sendRedirect("/COMP3095LabTest1C/loginError.html");
		else if (Credentials.ValidateUsername(username) && Credentials.ValidatePassword(password))
			response.sendRedirect("/COMP3095LabTest1C/order.html");
		else
			response.sendRedirect("/COMP3095LabTest1C/loginError.html");
	}

}
