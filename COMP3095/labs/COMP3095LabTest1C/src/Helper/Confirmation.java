package Helper;

import java.util.Random;

public class Confirmation {
	protected static int min = 1000000,
				  max = 9999999;
	
	// Confirmation Code Generation
	public static int GenerateCode() {
		Random rand = new Random();
	    int confirmationCode = rand.nextInt((max - min) + 1) + min;

	    return confirmationCode;
	}
}
