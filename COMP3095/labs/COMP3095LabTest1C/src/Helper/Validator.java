package Helper;

public class Validator {
	protected static String visaPattern = "^4[0-9]{12}(?:[0-9]{3})?$",
					 mastercardPattern = "^5[1-5][0-9]{14}$",
					 amexPattern = "^3[47][0-9]{13}$",
					 emailPattern = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$",
					 postalCodePattern = "^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$";
	
	public static String GetCreditCardRedirect(String creditcard) {
		if (creditcard.matches(visaPattern))
			return "https://www.visa.com/";
		else if (creditcard.matches(mastercardPattern))
			return "https://www.mastercard.com/";
		else if (creditcard.matches(amexPattern))
			return "https://www.americanexpress.com/";
		else
			return "404_NotFound";
	}
	
	public static boolean ValidateEmail(String email) {
		return email.matches(emailPattern);
	}
	
	public static boolean ValidatePostalCode(String postalcode) {
		return postalcode.matches(postalCodePattern);
	}
}
