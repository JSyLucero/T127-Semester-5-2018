package Helper;

public class Values {
	public static boolean isMissing(String s) {
		try {
			return (s.isEmpty() || s.length() == 0);
		} catch (NullPointerException ex) {
			return true;
		}
	}
}
