# T127 Semester 5 2018 Course Labs
## Jullian Anthony Sy-Lucero | jullian.sy-lucero@georgebrown.ca
## Student ID: 100998164

### Labs
#### [Week 1](https://github.com/JSyLucero/COMP3123_FSDev_Labs/tree/master/week1/nobelab)
#### [Week 2](https://github.com/JSyLucero/COMP3123_FSDev_Labs/tree/master/week2/)
#### [Week 3](https://github.com/JSyLucero/COMP3123_FSDev_Labs/tree/master/week3/)
