# T127 Semester 5 2018 Course Labs
## Jullian Anthony Sy-Lucero | jullian.sy-lucero@georgebrown.ca
## Student ID: 100998164

### Courses
#### [COMP3074 - Mobile Application Development I](https://gitlab.com/JSyLucero/T127-Semester-5-2018/tree/master/COMP3074/labs)
#### [COMP3122 - AI with Python](https://gitlab.com/JSyLucero/T127-Semester-5-2018/tree/master/COMP3122/labs)
#### [COMP3123 - Full Stack Development I](https://gitlab.com/JSyLucero/T127-Semester-5-2018/tree/master/COMP3123/labs)